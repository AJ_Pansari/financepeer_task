import csv
import sqlite3 as sql
from flask import session
from passlib.hash import sha256_crypt



def upload_data(data):
	for row in data:
		if(row['userId'] == None or row['id'] == None or row['title'] == None or row['body'] == None):
			message="JSON not in required format. Please check."
			return message
		userId=row['userId']
		record_id=row['id']
		title=row['title']
		body=row['body']
		try:
			with sql.connect("database.db") as con:
				con.row_factory = sql.Row
				cur = con.cursor()
				to_db=[userId,record_id,str(title),str(body)]
				cur.execute("INSERT into jsondata values(?,?,?,?)",to_db)
				con.commit()
				# return (poll_id)
		except Exception as e:
			print(e)
			message="Unexpected Database error : " + str(e)
			return message
	message="Uploaded"
	return message

def get_records():
	try:
		with sql.connect("database.db") as con:
			con.row_factory = sql.Row
			cur = con.cursor()
			sqlQuery="SELECT * from jsondata"
			print(sqlQuery)
			cur.execute(sqlQuery)
			records = cur.fetchall()
			print (records)
			return records
			con.commit()
	except Exception as e:
		print(e)
		return ([])

def authenticate(request):
	con = sql.connect("database.db")
	username = request.form['log']
	password = request.form['pwd']
	sqlQuery = "select password,profile_pic from authenticate where username = '%s'"%username
	cursor = con.cursor()
	cursor.execute(sqlQuery)
	row = cursor.fetchone()
	status = False
	if row:
		status = sha256_crypt.verify(password,row[0])
		if status:
			msg = username + "has logged in successfully"
			session['username'] = username
			session['profile_pic']=row[1]
			session['admin']=False
			session['firstname']= username
			print(session)
		else:
			msg = username + "login failed"

	return status					