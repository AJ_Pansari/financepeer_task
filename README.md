# Financepeer_task

FinancePeer Task Web App

## To be installed :
* Flask : sudo pip3 install Flask
* Jinja2 : sudo pip3 install Jinja2
* Sqlite3 : sudo apt install sqlite3

## To Run :
* delete the existing database if needed.
* sqlite3 database.db < initial.sql
* python3 initial.py #to enter the only login-password combination to the list of username-passwords (sha256 protected)
* python3 main.py
* open the generated localhost url in any browser

## Database :
* Two tables :
  * authenticate
    * contains the details of user for login (userId, Username, password(hash), profile_pic(exists or not))
  * jsondata
    * contains the JSON data in the format shared by FinancePeer. (userId, id, title, body)
    * Foreign Key on userId
    * (userId,id) as Primary Key

### Logging In is necessary to upload JSON file and view data. The tabs for them are visible only after login.
