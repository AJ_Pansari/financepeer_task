from flask import *
from functools import wraps
from data import *
import re
import json
import os

app = Flask(__name__)
logged_in=False
app.secret_key = 'MKhJHJH798798ksdkjhkjGHh'

app.config['MAX_CONTENT_LENGTH'] = 1024 * 1024
app.config['UPLOAD_EXTENSIONS'] = ['.json']


@app.route("/",methods=["GET","POST"])
def index():
	if logged_in:
		return render_template("project.html",cls_type="home page",logged_in=logged_in,det=session,title="FinancePeer | Intern Hiring Task")
	else :
		return render_template("project.html",cls_type="home page",logged_in=logged_in,title="FinancePeer | Intern Hiring Task")

@app.route('/login',methods=['POST','GET'])
def login():
	global logged_in
	session.clear()
	if request.method == 'GET':
		return render_template("login.html")
	if request.method == 'POST':
		next = request.values.get('next')
		if(authenticate(request)==False):
			return render_template("login.html",cls_type="home page",logged_in=logged_in,title="FinancePeer | Intern Hiring Task")
		if(authenticate(request)==True):
			logged_in=True
			if not next:
				return render_template("project.html",cls_type="home page",logged_in=logged_in,title="FinancePeer | Intern Hiring Task",det=session)
			else:
				return redirect(next,logged_in=logged_in,det=session)


@app.route('/logout',methods=['POST','GET'])
def logout():
	global logged_in
	logged_in=False
	session.clear()
	print(session)
	return render_template("project.html",cls_type="home page",logged_in=logged_in,title="FinancePeer | Intern Hiring Task")




@app.route("/upload",methods=["GET","POST"])
def upload():
	if logged_in:
			
		return render_template("upload.html",logged_in=logged_in,det=session)
	else :
		message="Access Denied! Please Log In."
		return render_template("error.html",message=message,logged_in=logged_in)


@app.route("/uploadfile",methods=["GET","POST"])
def uploadfile():
	if logged_in:
		file= request.files['inputFile']
		file_ext=os.path.splitext(file.filename)[1]
		print(file_ext)
		if file_ext not in current_app.config['UPLOAD_EXTENSIONS']:
		# if not re.match(r".*[.json]","file.filename"):
			print(file_ext)
			message="Only JSON file accepted. Try again !"
			return render_template("error.html",message=message,logged_in=logged_in,det=session)

		json_file = file.read()
		# print(json_file)
		data = json.loads(json_file.decode('utf-8'))
		print(data)

		message=upload_data(data)

		if(message is not "Uploaded"):
			return render_template("error.html",message=message,logged_in=logged_in,det=session)
			
		return render_template("message.html",message="Data parsed and Uploaded successfully.",logged_in=logged_in,det=session)
	else :
		message="Access Denied! Please Log In."
		return render_template("error.html",message=message,logged_in=logged_in)

@app.route("/view_table",methods=["GET","POST"])
def view_table():
	if logged_in:
		records=get_records()
		
		return render_template("view_table.html",records=records,logged_in=logged_in,det=session)
	else :
		message="Access Denied! Please Log In."
		return render_template("error.html",message=message,logged_in=logged_in)

'''
@app.route("/alerts",methods=["GET","POST"])


@app.route("/reviews",methods=["GET","POST"])
'''
app.run(debug=True)