drop table if exists authenticate ;
drop table if exists jsondata ;

create table authenticate (
userId int primary key not null UNIQUE,
username text not null,
password text not null,
profile_pic boolean default 0
);

create table jsondata (
userId int not null,
id int not null,
title varchar[50] not null,
body TEXT,
FOREIGN KEY (userId) REFERENCES authenticate(username),
PRIMARY KEY (userId,id)
);
