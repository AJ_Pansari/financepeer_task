import csv
import sqlite3 as sql
from passlib.hash import sha256_crypt


with sql.connect("database.db") as con:
	cur = con.cursor()
	temp = sha256_crypt.encrypt("p@ssw0rd")
	to_db = [1,"financepeer",temp,1]
	cur.execute("INSERT INTO authenticate (userId,username,password,profile_pic) VALUES (?,?,?,?);",to_db)
	con.commit()